package br.com.Itau.Desafio2.service;

import br.com.Itau.Desafio2.model.Lancamento;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServiceLancamento {

    private List<Lancamento> lancamentos;

    public List<Lancamento> find() {
        consumeAPI();
        return lancamentos;
    }
    public Lancamento findById(long id) {
        consumeAPI();
        return lancamentos.stream().filter(l -> id == l.getId()).collect(Collectors.toList()).get(0);
    }

    public List<Lancamento> findByCategoria(long categoria) {
        consumeAPI();
        return lancamentos.stream()
                .collect(Collectors.groupingBy(Lancamento::getCategoria,Collectors.toList()))
                .get(categoria);
    }


    public void consumeAPI() {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Lancamento>> rateResponse =
                restTemplate.exchange("https://desafio-it-server.herokuapp.com/lancamentos",
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Lancamento>>() {
                        });
        this.lancamentos = rateResponse.getBody();
    }

}
