package br.com.Itau.Desafio2.service;

import br.com.Itau.Desafio2.model.Categoria;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServiceCategoria {

    private List<Categoria> categorias;

    public List<Categoria> find() {
        consumeAPI();
        return categorias;
    }
    public Categoria findById(long id) {
        consumeAPI();
        return categorias.stream().filter(c -> id == c.getId()).collect(Collectors.toList()).get(0);
    }

    public void consumeAPI() {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Categoria>> rateResponse =
                restTemplate.exchange("https://desafio-it-server.herokuapp.com/categorias",
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Categoria>>() {
                        });
        this.categorias= rateResponse.getBody();
    }
}
