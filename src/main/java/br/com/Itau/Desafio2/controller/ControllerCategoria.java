package br.com.Itau.Desafio2.controller;

import br.com.Itau.Desafio2.model.Categoria;
import br.com.Itau.Desafio2.service.ServiceCategoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
public class ControllerCategoria {

    private static final Logger logger = Logger.getLogger(ControllerCategoria.class.toString());

    @Autowired
    private ServiceCategoria serviceCategoria;

    @GetMapping(path = "/categorias")
    public String find() {
        if(serviceCategoria.find().isEmpty()) {
            return ResponseEntity.notFound().build().toString();
        }
        logger.info(serviceCategoria.find().stream()
                .sorted(Comparator.comparing(Categoria::getId))
                .collect(Collectors.toList()).toString());

        return serviceCategoria.find().stream()
                .sorted(Comparator.comparing(Categoria::getId))
                .collect(Collectors.toList()).toString();
    }

    @GetMapping(value = "/categorias", params = "id")
    public String findById(@RequestParam(name = "id") Long id) {
        if(serviceCategoria.findById(id) == null) {
            return ResponseEntity.notFound().build().toString();
        }
        logger.info(serviceCategoria.findById(id).toString());

        return serviceCategoria.findById(id).toString();
    }
}
