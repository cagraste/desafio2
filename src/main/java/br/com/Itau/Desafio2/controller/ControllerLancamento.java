package br.com.Itau.Desafio2.controller;

import br.com.Itau.Desafio2.model.Lancamento;
import br.com.Itau.Desafio2.service.ServiceLancamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.logging.Logger;
import java.util.stream.Collectors;


@RestController
public class ControllerLancamento {

    private static final Logger logger = Logger.getLogger(ControllerLancamento.class.toString());

    @Autowired
    private ServiceLancamento serviceLancamento;


    @CrossOrigin(maxAge = 3600)
    @GetMapping(path = "/lancamentos")
    public String find() {
        if(serviceLancamento.find().isEmpty()) {
            return ResponseEntity.notFound().build().toString();
        }
        logger.info(serviceLancamento.find().stream()
                .sorted(Comparator.comparing(Lancamento::getmes_lancamento))
                .collect(Collectors.toList()).toString());

        return serviceLancamento.find().stream()
                .sorted(Comparator.comparing(Lancamento::getmes_lancamento))
                .collect(Collectors.toList()).toString();
    }

    @GetMapping(value = "/lancamentos", params = "id")
    public String findById(@RequestParam(name = "id") Long id) {
            if(serviceLancamento.findById(id) == null) {
                return ResponseEntity.notFound().build().toString();
            }
        logger.info(serviceLancamento.findById(id).toString());

        return serviceLancamento.findById(id).toString();
    }

    @GetMapping(value = "/lancamentos", params = "categoria")
    public String findByCategoria(@RequestParam(name = "categoria") Long categoria) {
        if(serviceLancamento.findByCategoria(categoria) == null) {
            return ResponseEntity.notFound().build().toString();
        }
        logger.info(serviceLancamento.findByCategoria(categoria).toString());

        return serviceLancamento.findByCategoria(categoria).toString();
    }

}
