package br.com.Itau.Desafio2.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Lancamento {
    private Long id;
    private Double valor;
    private String origem;
    private Long categoria;
    private Integer mes_lancamento;


    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Double getValor() {
        return valor;
    }
    public void setValor(Double valor) {
        this.valor = valor;
    }
    public String getOrigem() {
        return origem;
    }
    public void setOrigem(String origem) {
        this.origem = origem;
    }
    public Long getCategoria() {
        return categoria;
    }
    public void setCategoria(Long categoria) {
        this.categoria = categoria;
    }
    public Integer getmes_lancamento() {
        return mes_lancamento;
    }
    public void setMes_lancamento(Integer mes_lancamento) {
        this.mes_lancamento = mes_lancamento;
    }

    @Override
    public String toString() {
        return "{\"id\":" + id + ", \"valor\":" + valor + ", \"origem\":\"" + origem + "\", \"categoria\":" + categoria
                + ", \"mes_lancamento\":" + mes_lancamento + "}";
    }

}
