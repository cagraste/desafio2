package br.com.Itau.Desafio2;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class Desafio2ApplicationTests {


	@LocalServerPort
	private int port;


	@Autowired
	private TestRestTemplate restTemplate;


	@Test
	@Order(1)
	public void testFindAllLancamentos() {

		ResponseEntity<String> responseEntity = this.restTemplate
				.getForEntity("http://localhost:" + port + "/lancamentos", String.class);

		assertEquals(200, responseEntity.getStatusCodeValue());
	}

	@Test
	@Order(2)
	public void testFindLancamentobyId() {

		ResponseEntity<String> responseEntity = this.restTemplate
				.getForEntity("http://localhost:" + port + "/lancamentos?id=1", String.class);

		assertEquals(200, responseEntity.getStatusCodeValue());
	}

	@Test
	@Order(3)
	public void testFindLancamentobyCategoria() {

		ResponseEntity<String> responseEntity = this.restTemplate
				.getForEntity("http://localhost:" + port + "/lancamentos?categoria=1", String.class);

		assertEquals(200, responseEntity.getStatusCodeValue());
	}

	@Test
	@Order(4)
	public void testFindAllCategorias() {

		ResponseEntity<String> responseEntity = this.restTemplate
				.getForEntity("http://localhost:" + port + "/categorias", String.class);

		assertEquals(200, responseEntity.getStatusCodeValue());
	}

	@Test
	@Order(5)
	public void testFindCategoriaById() {

		ResponseEntity<String> responseEntity = this.restTemplate
				.getForEntity("http://localhost:" + port + "/categorias?id=1", String.class);

		assertEquals(200, responseEntity.getStatusCodeValue());
	}
}
